package com.epam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);
    public static void main(String[] args) {

        int[] numbers1;
        int[] numbers2;
        int[] numbersA;
        int[] numbersB;
        int[] numbersC;
        int[] resultA;
        int[] resultB;
        int[] resultC;
        int maxLength;
        int k;
        boolean check = false;


        numbers1 = new int[]{15, 15, 15, 15, 32, 32, 15, 4, 8, 9, -2, -8, 0, 41, 4, 12, 4, 0};
        numbers2 = new int[]{15, 15, 14, -3, 7, 4, 15, 3, -2};
        logger.debug("\nArray1: ");
        for (int i : numbers1) {
            logger.debug(i + " ");
        }
        logger.debug("\nArray2: ");
        for (int i :numbers2) {
            logger.debug(i + " ");
        }
//   Task A
        maxLength = (numbers1.length > numbers2.length) ? numbers1.length : numbers2.length;
        numbersA = new int[maxLength];
        k = 0;
        for (int i = 0; i < numbers1.length; i++) {
            for (int j = 0; j < numbers2.length; j++) {
                if (numbers1[i] == numbers2[j]) {
                    for (int l = 0; l < maxLength; l++) {
                        if (numbers1[i] == numbersA[l]) {
                            check = true;
                        }
                    }
                    if (!check) {
                        numbersA[k] = numbers1[i];
                        k++;
                    } else check = false;
                }
            }
        }

        resultA = new int[k];
        logger.info("\nResultA array: ");
        for (int i = 0; i < k; i++) {
            resultA[i] = numbersA[i];
            logger.info(resultA[i] + " ");
        }


//   Task B
        k = 0;
        numbersB = new int[numbers1.length];
        for (int i = 0; i < numbers1.length; i++) {
            int count = 0;
            for (int j = 0; j < numbers1.length; j++) {
                if (numbers1[j] == numbers1[i]) {
                    count++;
                }
            }
            if (count < 3) {
                numbersB[k] = numbers1[i];
                k++;
            }
        }

        resultB = new int[k];
        logger.info("\nresultB array: ");
        for (int i = 0; i < k; i++) {
            resultB[i] = numbersB[i];
            logger.info(resultB[i] + " ");
        }

//   Task C
        numbersC = new int[numbers1.length];
        k = 0;
        for (int i = 0; i < numbers1.length - 1; i++) {
            if (numbers1[i] != numbers1[i + 1]) {
                numbersC[k] = numbers1[i];
                k++;
            }
        }
        if (numbers1[numbers1.length-1] != numbers1[numbers1.length-2]) {
            numbersC[k] = numbers1[numbers1.length-1];
            k++;
        }

        resultC = new int[k];
        logger.info("\nResultC array: ");
        for (int i = 0; i < k; i++) {
            resultC[i] = numbersC[i];
            logger.info(resultC[i] + " ");
        }
    }
}